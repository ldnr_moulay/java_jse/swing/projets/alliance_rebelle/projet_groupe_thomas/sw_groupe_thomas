package Interface;
import javax.swing.JFrame;
import java.awt.TextField;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import Metier.Pilote;
import javax.swing.JOptionPane;

/**
 * crée un champ qui demande l'identifiant du pilote et un bouton ok
 * Une fois le champ rempli et qu'on clique sur le bouton ok on affiche les informations d'un pilote sur une nouvelle fenetre
 *
 * @author Samuel
 */
public class AfficherPilote 
{
    JFrame fenetre;
    JFrame fenetre2;
    int numero;
    TextField champNum;
    JPanel jp;
    JButton valider;
    JOptionPane jopt;
    public AfficherPilote()
    {
        // initialisation des variables d'instance
        fenetre=new JFrame("Demande identifiant du pilote");
        fenetre.setSize(300,100);
        fenetre.setLayout(new FlowLayout());
        fenetre.add(new JLabel("numéro d'identifiant: "));
        champNum=new TextField();
        champNum.setColumns(5);
        fenetre.add(champNum);
        valider=new JButton("OK");
        fenetre.add(valider);
        valider.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                afficherPilote(Integer.parseInt(champNum.getText()));
                fenetre.dispose();
            }
        });
        fenetre.setVisible(true);
        
    }
    
    private void afficherPilote(int num){
        try{Pilote pilote=Pilote.getPiloteNum(num);
        fenetre2=new JFrame("Fiche du pilote numéro: "+num);
        fenetre2.setSize(400,200);
        fenetre2.setLayout(new FlowLayout());
        fenetre2.add(new JLabel("Prenom:"));
        fenetre2.add(new JLabel(pilote.getPrenom()));
        fenetre2.add(new JLabel("Nom:"));
        fenetre2.add(new JLabel(pilote.getNom()));
        fenetre2.add(new JLabel("Race:"));
        fenetre2.add(new JLabel(pilote.getRace()));
        fenetre2.add(new JLabel("Age:"));
        fenetre2.add(new JLabel(pilote.getAge()+""));
        fenetre2.add(new JLabel("associé au chasseur:"));
        fenetre2.add(new JLabel(pilote.getChasseur().getNumero()+""));
        fenetre2.setVisible(true);}
        catch(Exception e){
            jopt.showMessageDialog(null, "Pilote non trouvé","Erreur d'identifiant", JOptionPane.ERROR_MESSAGE);
            new AfficherPilote();
        }
    }
}
