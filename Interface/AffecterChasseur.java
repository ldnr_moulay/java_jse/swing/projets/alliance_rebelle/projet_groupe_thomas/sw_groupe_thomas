package Interface;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import Metier.Chasseur;
import Metier.Pilote;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;


/**
 * Affecte un pilote à un Chasseur
 * La variable pilote de la classe chasseur contient après la fonction un pilote, et pareil pour Chasseur pour son champ chasseur
 *
 * @author Samuel
 */

public class AffecterChasseur
{
    JFrame fenetre1;
    JPanel jp;
    JButton bOK1;
    JTextField numPUser,numCUser;
    JOptionPane jopt;
    
    public AffecterChasseur(){
        fenetre1=new JFrame("Affectation d'un chasseur au pilote");
        fenetre1.setSize(300,120);
        fenetre1.setLayout(new FlowLayout());
        fenetre1.add(new JLabel("Vous voulez associer le pilote n°"));
        numPUser=new JTextField();
        fenetre1.add(numPUser);
        fenetre1.add(new JLabel("au chasseur n°"));
        numCUser=new JTextField();
        numCUser.setColumns(5);
        numPUser.setColumns(5);
        fenetre1.add(numCUser);
        bOK1=new JButton("OK");
        fenetre1.add(bOK1);
        bOK1.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                int numP,numC;
                numP=Integer.parseInt(numPUser.getText());
                numC=Integer.parseInt(numCUser.getText());
                affectation(numP,numC);
            }
        });
        fenetre1.setVisible(true);
    }
    
    private void affectation(int numP,int numC){
            try{
            Pilote pilote=Pilote.getPiloteNum(numP);
            pilote.affecterChasseur(numC);
        }
            catch(Exception e){
            jopt.showMessageDialog(null, e.getMessage(),"Erreur numéro ¨Pilote", JOptionPane.ERROR_MESSAGE);
    }
        try{
        Chasseur chasseur=Chasseur.getChasseurNum(numC);
        chasseur.affecterPilote(numP);
        jopt.showMessageDialog(null,Pilote.getPiloteNum(numP).getPrenom()+" a bien été associé au chasseur "+numC,"Information",JOptionPane.INFORMATION_MESSAGE);
    }
        catch(Exception e){
        jopt.showMessageDialog(null, e.getMessage(),"Erreur numéro Chasseur", JOptionPane.ERROR_MESSAGE);
    }
        fenetre1.dispose();
    }
}
