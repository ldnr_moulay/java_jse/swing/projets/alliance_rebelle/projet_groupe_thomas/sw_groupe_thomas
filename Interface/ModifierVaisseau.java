package Interface;


import java.awt.*;
import javax.swing.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.Map;
import java.util.HashMap;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import Metier.*;

/**
* Crée une fenetre avec un champ pour le numéro du chasseur et un menu déroulant pour sélectioner un état parmis 3
* Quand on clique sur valider le chasseur identifié modifie son état pour celui selectionné
*
* @author (Thomas)
* @version (04-05-2021)
*/
public class ModifierVaisseau extends JFrame
{
    private Panel fenetre = new Panel();
    private JPanel conteneur = new JPanel();
    private JLabel titre = new JLabel("Modification du vaisseau");
    
    private JTextField choixIdentifiant = new JTextField("0");
    private JButton bouton = new JButton("Valider");
    
    private JLabel etatTxt = new JLabel("L'état du vaisseau ");
    private String[] tab ={"Opérationnel", "En maintenance", "Détruit"};
    private JComboBox etat = new JComboBox(tab);
    
    private String etatVaisseau;
    private Integer saisie;



    public ModifierVaisseau(){
        
        this.setSize(400,300);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        conteneur.setBackground(Color.white);
        conteneur.setLayout(new BorderLayout());
        
        // fenetre.setBackground(Color.blue);
        fenetre.setLayout(new BorderLayout());
        
        JPanel top = new JPanel();
        top.add(titre);
        
        // on déclare le bouton valider
        
        JPanel south = new JPanel();
        south.add(bouton);
        conteneur.add(south, BorderLayout.SOUTH);
        
        // on crée les panneaux
        JPanel identifiant = new JPanel();
        JPanel listeEtat = new JPanel();
        
        // on demande à l'utilisateur de donner l'identifiant du chasseur
        
        choixIdentifiant.setPreferredSize(new Dimension(150,30));
        identifiant.add(choixIdentifiant);
        // l'utilisateur choisi parmis les états proposés
        
        
        etat.setPreferredSize(new Dimension(200,40));
        listeEtat.add(etat);
        // assemblage des fenetres
        JPanel middle =new JPanel();
        middle.setLayout(new BorderLayout());
        
        middle.add(identifiant, BorderLayout.NORTH);
        middle.add(etat, BorderLayout.CENTER);
        
        fenetre.add(top,BorderLayout.NORTH);
        fenetre.add(middle,BorderLayout.CENTER);
        conteneur.add(fenetre,BorderLayout.CENTER);
        
        this.setContentPane(conteneur);
        

        bouton.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e){
            
                valider();
            
                }
        });
    
    
    
        this.setVisible(true);
        }
        
    public void valider(){
    
        JOptionPane jop1;
        try{
        saisie = Integer.parseInt(choixIdentifiant.getText());
        }catch(Exception e){
        
            jop1 = new JOptionPane();
            jop1.showMessageDialog(null,"Vous n'avez pas mis un entier","Message d'erreur",
            JOptionPane.ERROR_MESSAGE);
            this.dispose();
            new ModifierVaisseau();
        }
        
        try{
            Chasseur.modifierEtat(etat.getSelectedItem().toString(), saisie);
        
        }catch(Exception e){
        
            jop1 = new JOptionPane();
            jop1.showMessageDialog(null,"Il n'y a pas de chasseurs pour cet identifiant","Message d'erreur",
            JOptionPane.ERROR_MESSAGE);
            this.dispose();
            new ModifierVaisseau();
            }
    
    }

} 
