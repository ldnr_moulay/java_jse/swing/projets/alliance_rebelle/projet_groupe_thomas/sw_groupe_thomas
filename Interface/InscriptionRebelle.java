package Interface;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JDialog;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Metier.Pilote;
import java.util.Set;

/**
 * Crée un champ prénom un champ Nom un menu déroulant race, un champ age et un bouton valider
 * Quand on appuie sur le bouton valider on crée unb pilote avec les champs saisis
 *
 * @author Samuel_Lefranc
 * Création de le fenetre qui demandera les données du nouveau pilote pour 
 * inscrire le rebelle à la formaiton pilote
 */
public class InscriptionRebelle
{
    JOptionPane jopt;
    JFrame fenetre;
    JPanel l1,l2;
    JTextField nomUser,prenomUser,ageUser;
    JComboBox races;
    String[] tabRaces;    
    JButton bEnregistrer;
    public InscriptionRebelle(){
        fenetre=new JFrame("Nouveau pilote");
        fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        fenetre.setSize(500,150);
        fenetre.setLayout(new BoxLayout(fenetre.getContentPane(),BoxLayout.PAGE_AXIS));
        l1=new JPanel();
        fenetre.add(l1);
        l1.setLayout(new FlowLayout());
        l1.add(new JLabel("Prénom: "));
        l1.add(prenomUser=new JTextField());
        prenomUser.setColumns(15);
        l1.add(new JLabel("Nom: "));
        l1.add(nomUser=new JTextField());
        nomUser.setColumns(15);
        l2=new JPanel();
        fenetre.add(l2);
        l2.setLayout(new FlowLayout(FlowLayout.CENTER,30,3));
        //l2.setHgap(10);
        l2.add(new JLabel("Race: "));
        convertToTab(Pilote.getRaces());
        races=new JComboBox(tabRaces);
        l2.add(races);
        l2.add(new JLabel("Age: "));
        ageUser=new JTextField();
        l2.add(ageUser);
        ageUser.setColumns(3);
        bEnregistrer=new JButton("Enregistrer");
        l2.add(bEnregistrer);
        bEnregistrer.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e){enregistrer();}
            });
        /*races.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){}
        });*/
        fenetre.setVisible(true);
        
    }
    
    
    /**
     * Enregistre les champs et les associe à un nouveau pilote
     */
    private void enregistrer(){
        try{int age=ageToInt(ageUser.getText());
         
       
            Pilote pilote=new Pilote(prenomUser.getText(),nomUser.getText(),races.getSelectedItem().toString(),age);
            //System.out.println(nomUser.getText()+prenomUser.getText()+age+races.getSelectedItem()+"");
            jopt.showMessageDialog(null, "Pilote créé ","Information", JOptionPane.INFORMATION_MESSAGE);
            fenetre.dispose();
        }catch(Exception e){
            jopt.showMessageDialog(null, e.getMessage(),"Erreur d'age", JOptionPane.ERROR_MESSAGE);
            //new InscriptionRebelle();
        }
    }
    
    private String[] convertToTab(Set set){
        Pilote.listeRaces();
        tabRaces=new String[Pilote.getRaces().size()];
        int i=0;
        for(String race: Pilote.getRaces()){
            tabRaces[i]=race;
            i++;
        }
        return tabRaces;
    }
    private int ageToInt(String stringAge) throws Exception{
        try{return Integer.parseInt(ageUser.getText());}
        catch(Exception e){throw new Exception("Erreur sur l'age");}
    }
}