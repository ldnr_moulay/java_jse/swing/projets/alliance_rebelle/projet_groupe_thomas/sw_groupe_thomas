package Interface;
import java.awt.*;
import javax.swing.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.Map;
import java.util.HashMap;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import Metier.*;

/**
* Affiche les identififiants des vaisseaux opérationnels
*
* @author (Thomas)
* @version (04-05-2021)
*/
public class AfficherVaisseauOperationnel extends JFrame
{
private Panel fenetre = new Panel();
private JPanel conteneur = new JPanel();
private JLabel titre = new JLabel("Vaisseaux Opérationnels");
private ArrayList<Integer> operationnel = new ArrayList<>();
private JLabel nbrChasseurTxt = new JLabel("Nombre de vaisseaux opérationnels : ");
private JLabel nbrChasseur = new JLabel();
private JLabel identifiantTxt = new JLabel("Liste des identifiants des vaisseaux : ");

public AfficherVaisseauOperationnel(){

this.setTitle("Nombre de vaisseaux opérationnels");
this.setSize(500,300);
this.setLocationRelativeTo(null);
this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

fenetre.setLayout(new BorderLayout());
conteneur.setLayout(new GridLayout(3,1));

// affichage du nombre de vaisseaux

operationnel = Chasseur.afficherListeChasseurOperationnels();

nbrChasseur.setText("" + operationnel.size());

JPanel nbrVaisseau = new JPanel();

nbrVaisseau.add(nbrChasseurTxt);
nbrVaisseau.add(nbrChasseur);
JPanel IdentifiantTxt = new JPanel();
IdentifiantTxt.add(identifiantTxt);
conteneur.add(nbrVaisseau);
conteneur.add(IdentifiantTxt);
JPanel Identifiants = new JPanel();
Identifiants.setLayout(new BoxLayout(Identifiants,BoxLayout.PAGE_AXIS));

for(Integer c : operationnel){

Identifiants.add(new JLabel(c+""));
}
JPanel identifiant = new JPanel();
identifiant.add(Identifiants);
conteneur.add(identifiant);



// assemblage des fenetres

fenetre.add(conteneur,BorderLayout.CENTER);

this.setContentPane(conteneur);
this.setVisible(true);
}
} 