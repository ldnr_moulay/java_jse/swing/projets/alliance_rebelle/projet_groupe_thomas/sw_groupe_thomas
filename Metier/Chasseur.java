package Metier;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;


/**
 * Classe pour créer des objets Chasseur
 * @author (Chantal)
 * @version (1.0----03-05-2021)
 */
public class Chasseur
{
    private TypeVaisseau type;
    private int numero;
    private String etat;
    private static Map < Integer, Chasseur> chasseur=new HashMap<>();
    private  Pilote pilote;

    /**
     * Constructeur d'objets de classe Chasseur
     */
    public Chasseur(TypeVaisseau type, String etat)
    {
       this.type = type;
       this.etat = etat;
       
       this.numero=chasseur.size()+1;
       chasseur.put(this.numero,this);
    }
    /**
     * Getters et setters
     */
    public int getNumero()
    {
       return this.numero;
    }
    
     public String getEtat()
    {
       return this.etat;
    }
    
     public TypeVaisseau getType ()
    {
       return type;
    }
    
   public void setNumero()
    {
       this.numero=numero;
    }
    
     public void setEtat(String etat)
    {
        this.etat = etat;
    }
    public static void modifierEtat(String etat, int numC) throws Exception
    {
        boolean trouve=false;
    for (Map.Entry<Integer, Chasseur> entry: chasseur.entrySet()){
        if(entry.getKey()==numC){
            
            entry.getValue().setEtat(etat);
            
            trouve=true;
        }
    }
    if(!trouve){throw new Exception("Identifiant non trouvé");}



} 
    
     public void setType()
    {
        this.type = type;
    }


    /**
     *Methode qui va afficher la liste des chasseurs
     */
    public void afficher ()
    {
        
    }
    
    public static Chasseur getChasseurNum(int numC) throws Exception{
                for (Map.Entry<Integer, Chasseur> entry: chasseur.entrySet()){
            if(entry.getKey()==numC){return entry.getValue();}
            
        }
        throw new Exception("Chasseur non trouvé");
    }
    
    public int stringChasseur(){
        return this.getNumero();
    }
    
    public Pilote getPilote(){
        return pilote;
    }
        
     /**
     *Methode qui va permettre de modifier les états 
     */
    public static Map getListeChasseur()
    {
        return chasseur;
    }
    
    public void affecterPilote(int numP) throws java.lang.Exception {
        try{
            this.pilote=Pilote.getPiloteNum(numP);
            
        }catch(Exception e){
            throw e;
        }
    }
    
     /**
     *Methode qui va permettre d'afficher les chasseurs qui ont un 
     *état opérationnel
     */
    public static ArrayList afficherListeChasseurOperationnels()
{

    ArrayList<Integer> ope = new ArrayList<>();
    for(Map.Entry<Integer,Chasseur> entry: chasseur.entrySet()){
        if(entry.getValue().getEtat().equals("Opérationnel")){
            ope.add(entry.getKey());
        }
    }
    
        /*for(int i = 1; i <= chasseur.size(); i++){
        
            if(chasseur.get(i).getEtat().equals("Opérationnel")){
                ope.add(i);
            }
        }*/
    return ope;
    } 
}
