package Metier;
/**
 * Classe abstraite mère de X-Wing et Y-Wing
 * @author (Chantal)
 */
public abstract class TypeVaisseau
{
    private String nomType;

    public abstract String getNom();
    
}
