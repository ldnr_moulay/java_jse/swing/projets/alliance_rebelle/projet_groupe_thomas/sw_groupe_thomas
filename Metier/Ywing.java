package Metier;
/**
 * Classe pour donner un type à un vaisseau, peut donner son nom
 *
 * @author (Chantal)
 */
public class Ywing extends TypeVaisseau
{
    private String nomType;

    /**
     * Constructeur d'objets de classe Ywing
     */
    public Ywing()
    {
        nomType="Y-wing";
    }
    
    public String getNom(){
        return this.nomType;
    }
}
