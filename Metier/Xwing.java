package Metier;
/**
 * Classe pour donner un type à un vaisseau, peut donner son nom
 *
 * @author (Chantal)
 */
public class Xwing extends TypeVaisseau
{
    private String nomType;

    /**
     * Constructeur d'objets de classe Ywing
     */
    public Xwing()
    {
        nomType="X-Wing";
    }
    
    public String getNom(){
        return this.nomType;   
    }
}



